package com.epam.task4.arreys.SameElemAtOneArr;

public class FindSameElem {
    public static void FindSameElem() {
        int arr[] = {1, 2, 3, 2, 4, 5, 1, 3, 2};
        int arrSecondEl=0;
        int count;
        for (int i = 0; i < arr.length; i++) {
            count = 0;
            for (int j = 0; j < arr.length ; j++) {
                if (arr[i] == arr[j]) {
                    count++;
                    if(count == 2) {
                        arrSecondEl = j;
                    }
                    if (count >= 3) {
                        arr[i]=0;
                        arr[j]=0;
                        arr[arrSecondEl]=0;
                    }
                }
            }
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
