package com.epam.task4.arreys.SameElemAtOneArr;

public class FindSomeElem2 {
    public static void FindSameElem() {
        int arr[] = {1, 2, 3, 2, 4, 5, 1, 3, 2};
        int a, b, size;
        size = arr.length;
        int t;
        int sizeminus = -1;
        for (a = 1; a < size; a++) {
            for (b = size - 1; b >= a; b--) {
                if (arr[b - 1] > arr[b]) {
                    t = arr[b - 1];
                    arr[b - 1] = arr[b];
                    arr[b] = t;
                }
            }
        }
        for (int i = 0; i < size - 2; i++) {
            if (arr[i] == arr[i + 1] && arr[i + 1] == arr[i + 2]) {
                int After2Same = size-i-2-1;
                for (int m = 0; After2Same > 0; After2Same--) {
                    arr[i + m] = arr[size - After2Same];
                    m++;
                    sizeminus++;
                }
            }
        }
        System.out.println();
        for (int i = 0; i < size - sizeminus; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}


