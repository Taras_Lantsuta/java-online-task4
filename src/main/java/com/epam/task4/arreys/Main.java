package com.epam.task4.arreys;

import com.epam.task4.arreys.Game.GameMain;
import com.epam.task4.arreys.SameElemAtOneArr.FindSameElem;
import com.epam.task4.arreys.SameElemAtOneArr.FindSomeElem2;
import com.epam.task4.arreys.TwoArrMakeThird.SameElAtTwo;

public class Main {
    public static void main(String[] args) {
//        SameElAtTwo.SameElAtTwo();    //Дано два масиви...
//        FindSameElem.FindSameElem();  //Видалити в масиві всі числа, які повторюються більше двох разів.
                                      // (під видаленням розуміється заміна на 0).
//        FindSomeElem2.FindSameElem(); //Видалити в масиві всі числа, які повторюються більше двох разів.
        GameMain.Game();
    }
}
