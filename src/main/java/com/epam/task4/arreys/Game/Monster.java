package com.epam.task4.arreys.Game;

public class Monster {
    public static void Monster() {
        double MonsterPower = Math.random();
        MonsterPower = MonsterPower * 100;
        if(Hero.getHeroPower() < MonsterPower){
            Hero.setHeroPower(Hero.getHeroPower() - (int)MonsterPower);
        }
        System.out.println("Hero power after monster is: " + Hero.getHeroPower());
    }
}
