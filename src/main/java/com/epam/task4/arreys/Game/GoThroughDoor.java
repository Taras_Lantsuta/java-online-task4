package com.epam.task4.arreys.Game;

public class GoThroughDoor {
    public static void GoThroughDoor() {

        double MonsterOrArtifact = Math.random();
        if (MonsterOrArtifact >= 0.5) {
            Monster.Monster();
        } else {
            Artifact.Artifact();
        }
    }
}
