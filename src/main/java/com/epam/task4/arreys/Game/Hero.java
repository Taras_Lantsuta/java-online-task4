package com.epam.task4.arreys.Game;

public class Hero {
    private static int HeroPower = 25;

    public static int getHeroPower() {
        return HeroPower;
    }

    public static void setHeroPower(int heroPower) {
        HeroPower = heroPower;
    }
}
